/* SPDX-FileCopyrightText: 2022 Johannes Strelka-Petz <johannes_at_oskars.org> */
/* SPDX-License-Identifier: CERN-OHL-S-2.0+ */

/*  ------------------------------------------------------------------------------ */
/* | Copyright Johannes Strelka-Petz 2022.                                        | */
/* |                                                                              | */
/* | This source describes Open Hardware and is licensed under the CERN-OHL-S v2  | */
/* | or any later version.                                                        | */
/* |                                                                              | */
/* | You may redistribute and modify this source and make products using it under | */
/* | the terms of the CERN-OHL-S v2 or any later version                          | */
/* | (https://ohwr.org/cern_ohl_s_v2.txt).                                        | */
/* |                                                                              | */
/* | This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,          | */
/* | INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A         | */
/* | PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 or any later version        | */
/* | for applicable conditions.                                                   | */
/* |                                                                              | */
/* | Source location: https://gitlab.com/teamoskar/oskar_zither                   | */
/*  ------------------------------------------------------------------------------ */

// printer
printer = "prusa i3"; // [prusa i3, kossel]

// kailh_choc_PG1350
kailh_choc_PG1350_height=9.9;
echo(kailh_choc_PG1350_height=9.9);

// keycap
        keycap_h=3.2;
        keycap_x=17.5;
        keycap_y=16.5;
        
// kailh_choc_PG1350 raster
        kailh_choc_PG1350_dx=keycap_x+1;
        kailh_choc_PG1350_dy=keycap_y+1;

tolerance1=1;
tolerance2=0.5;


// keypad
keypad_size_x=kailh_choc_PG1350_dy*2;
echo(keypad_size_x=kailh_choc_PG1350_dy*2);
keypad_size_y=kailh_choc_PG1350_dx*4;
echo(keypad_size_y=kailh_choc_PG1350_dx*4);
keypad_size_z=kailh_choc_PG1350_height;
echo(keypad_size_z=kailh_choc_PG1350_height);

// lipo      
lipo_length=40;
lipo_width=25;
lipo_height=6.5;
lipo_protector_length=lipo_length-4.5;

//pin
pin_w=0.64;
pin_h=11.54;
pin_low=3;
pin_high=6;
pin_block_width=2.54;
pin_rm=2.54;

/* // lolind32 */
/* lolind32_length=57.5; */
/* echo(lolind32_length=57.5); */
/* lolind32_width=25.5; */
/* echo(lolind32_width=25.5); */
/* lolind32_pcb_height=1; */
/* lolind32_height=lolind32_pcb_height+6.5+pin_block_width; */
/* echo(lolind32_height=lolind32_pcb_height+6.5); */
/* lolind32_tolerance=0.2; */
/* lolind32_usb_h=2.5; */
/* lolind32_usb_w=7.5; */

wall=2;

// pcb
pcb_width=1.6;

//develop
//facetcount=8;
// finish
facetcount=24;

$fn=facetcount;

// kailh_choc_PG1350
pg1350_l=13.8;
pg1350_h=5;    
pg1350_ol=15;
pg1350_olh=0.8;
pg1350_cd=3.2;
pg1350_cd2=1.9;
pg1350_ch=2.65;
kailh_choc_PG1350_block_height=pg1350_h/2+4.2+keycap_h;

// thumbplate
tplate_tolerance= printer=="prusa i3" ? tolerance1/3 : tolerance2; // tolerance for side length of switch in tplate
echo (tplate_tolerance=tplate_tolerance);
plate_h=1.5-tolerance1/10;
module thumbplate(uncut=false,holderspace=0){
     moreplate_y=3;
     moreplate_x=5;
     difference(){
	  if (uncut){
	       translate([0,moreplate_y/2,(pg1350_h-pg1350_olh-plate_h)/2]){
		    cube([kailh_choc_PG1350_dx+moreplate_x+holderspace,kailh_choc_PG1350_dy+moreplate_y+holderspace,plate_h+holderspace], center=true);
		    }
	  }
	  else {
	       	  intersection(){
		       translate([0,moreplate_y/2,(pg1350_h-pg1350_olh-plate_h)/2]){
			    cube([kailh_choc_PG1350_dx+moreplate_x,kailh_choc_PG1350_dy+moreplate_y,plate_h], center=true);
		       }
		       translate([0,1,0]){rotate([0,0,45]){cube([26,26,10],center=true);}}
		  }
	  }
	  // kailh_choc_PG1350
	  translate([0,0,(pg1350_h-pg1350_olh-plate_h)/2]){
	       cube([pg1350_l+tplate_tolerance,pg1350_l+tplate_tolerance,plate_h*2],center=true);
	  }
     }
}
//thumbplate(uncut=true, holderspace=0);

holderspace= printer=="prusa i3" ? 0.2 : 0.5; // tolerance thumbplate in thumbplateholder
echo(holderspace=holderspace);
case_inner_hull_tolerance=tolerance1;
thumbkeycaptolerance=case_inner_hull_tolerance*2;
module thumbplateholder(){
     moreplate_x=5+10+8;
     moreplate_y=3+6;
     holder_z=9;
	  difference(){
	       translate([0,moreplate_y/2,(pg1350_h-pg1350_olh-plate_h)*0+holder_z/2-1]){
		    cube([kailh_choc_PG1350_dx+moreplate_x,kailh_choc_PG1350_dy+moreplate_y,holder_z], center=true);
	       }
	       kailh_choc_PG1350(tolerance=case_inner_hull_tolerance);
	       translate([0,0,plate_h+holderspace+kailh_choc_PG1350_block_height/2]){
		    kailh_choc_PG1350(tolerance=thumbkeycaptolerance);
	       }
	       thumbplate(uncut=true, holderspace=holderspace);
	  }
}
//thumbplate(uncut=true);
//kailh_choc_PG1350();
//thumbplateholder();

module kailh_choc_PG1350(tolerance=0,corner=false, thumbplate=false){
     if(thumbplate){thumbplate();}
     else
     if(corner){
	  translate([0,0,kailh_choc_PG1350_block_height/2-pg1350_ch/2-tolerance/2]){
	       // corner 2
	       translate([(keycap_x+tolerance)/2,(keycap_y+tolerance)/2,(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2]){
		    cube([1,1,1],center=true);
		    kailh_choc_PG1350_cornervector2=[0,0,kailh_choc_PG1350_block_height/2-pg1350_ch/2-tolerance/2]+
			 [(keycap_x+tolerance)/2,(keycap_y+tolerance)/2,(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2];
	       }
	       // corner 1
	       translate([-(keycap_x+tolerance)/2,(keycap_y+tolerance)/2,(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2]){
		    cube([1,1,1],center=true);
		    kailh_choc_PG1350_cornervector=[0,0,kailh_choc_PG1350_block_height/2-pg1350_ch/2-tolerance/2]+
			 [-(keycap_x+tolerance)/2,(keycap_y+tolerance)/2,(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2];
	            echo(kailh_choc_PG1350_cornervector=kailh_choc_PG1350_cornervector); // debug
	       }
	       translate([(keycap_x+tolerance)/2,-(keycap_y+tolerance)/2,(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2]){
		    *cube([1,1,1],center=true);
		    kailh_choc_PG1350_cornervector3=[0,0,kailh_choc_PG1350_block_height/2-pg1350_ch/2-tolerance/2]+
			 [(keycap_x+tolerance)/2,-(keycap_y+tolerance)/2,(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2];
	       }
	       translate([-(keycap_x+tolerance)/2,-(keycap_y+tolerance)/2,(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2]){
		    *cube([1,1,1],center=true);
	       }
	       translate([(keycap_x+tolerance)/2,(keycap_y+tolerance)/2,-(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2]){
		    *cube([1,1,1],center=true);
	       }
	       translate([-(keycap_x+tolerance)/2,(keycap_y+tolerance)/2,-(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2]){
		    *cube([1,1,1],center=true);
	       }
	       translate([(keycap_x+tolerance)/2,-(keycap_y+tolerance)/2,-(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2]){
		    *cube([1,1,1],center=true);
	       }
	       translate([-(keycap_x+tolerance)/2,-(keycap_y+tolerance)/2,-(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2]){
		    *cube([1,1,1],center=true);
	       }
	  }
	  //kailh_choc_PG1350_cornervector=[0,0,kailh_choc_PG1350_block_height/2-pg1350_ch/2-tolerance/2]+[-(keycap_x+tolerance)/2,(keycap_y+tolerance)/2,(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2];
	  //translate(kailh_choc_PG1350_cornervector){cube([2,2,2],center=true);}

     }
     else{
	  if(tolerance>0){
	       kailh_choc_PG1350_block_height=pg1350_h/2+4.2+keycap_h;
	       // keycap size + tolerance
	       // translate([0,0,kailh_choc_PG1350_block_height/2-pg1350_ch/2]){
	       // add tolerance at bottom but not at top
	       translate([0,0,kailh_choc_PG1350_block_height/2-pg1350_ch/2-tolerance/2]){
		    cube([keycap_x+tolerance,keycap_y+tolerance,kailh_choc_PG1350_block_height+pg1350_ch+tolerance],true);
	       }
	  }
	  else{

	       translate([0,0,pg1350_h/2]){
		    color("brown"){
			 cube([pg1350_l,pg1350_l,pg1350_h],true);
			 cube([pg1350_ol,pg1350_ol,pg1350_olh],true);
		    }
    
//  bolts  
		    translate([0,0,-pg1350_ch-pg1350_h/2]){
			 cylinder(pg1350_ch, d = pg1350_cd,false);
			 translate([5.5,0,0]){
			      cylinder(pg1350_ch, d = pg1350_cd2,false);
			 }
			 translate([-5.5,0,0]){
			      cylinder(pg1350_ch, d = pg1350_cd2,false);
			 }
		    }
//  keycap
		    translate([0,0,keycap_h/2+4.2]){
			 color("grey"){
			      cube([keycap_x,keycap_y,keycap_h],true);
			 }
		    }
	       }
	  
	  }
     }
}

// kailh_choc_PG1350(tolerance=1,corner=true);
//kailh_choc_PG1350(tolerance=0);
//kailh_choc_PG1350(tolerance=0,thumbplate=true);
tolerance=1;
// kailh_choc_PG1350_cornervector definition in module kailh_choc_PG1350
// kailh_choc_PG1350_cornervector=[0,0,kailh_choc_PG1350_block_height/2-pg1350_ch/2-tolerance/2]+[-(keycap_x+tolerance)/2,(keycap_y+tolerance)/2,(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2];
// translate(kailh_choc_PG1350_cornervector){cube([2,2,2],center=true);}

module keypad(tolerance=0){
     translate([kailh_choc_PG1350_dy/2,kailh_choc_PG1350_dx/2,0]){
	  for(row=[0:1]){
	       for(col=[0:3]){
		    translate([row*kailh_choc_PG1350_dy,col*kailh_choc_PG1350_dx,0]){
			 rotate([0,0,90]){
			      kailh_choc_PG1350(tolerance=tolerance);
			 }
                    }
	       }
	  }
	  
     }
}
/* keypad(tolerance=keypadtolerance*0); */
/* #keypad(tolerance=keypadtolerance); */


// cubot manito with hull
smartphone_x=76;
smartphone_y=148;
smartphone_z=12;
keypad_to_smartphonebottom=10;
switch_to_smartphoneback=smartphone_z+10;
smartphone_position_x=(-smartphone_x+keypad_size_x)/2;
smartphone_position_y=-keypad_to_smartphonebottom;
smartphone_position_z=-switch_to_smartphoneback;
module smartphone(tolerance=0){
     translate([smartphone_position_x-tolerance/2,smartphone_position_y-tolerance/2,smartphone_position_z-tolerance/2]){
	  cube([smartphone_x+tolerance,smartphone_y+tolerance,smartphone_z+tolerance]);
     }
}

//thumbs_position_y=kailh_choc_PG1350_dy*(4+1/2);
//thumbs_position_x_left=3*kailh_choc_PG1350_dx;
//thumbs_position_x_right=-2*kailh_choc_PG1350_dx;
thumbs_position_y=kailh_choc_PG1350_dy*(4+1/2)-2;
thumbs_position_y2=kailh_choc_PG1350_dy*(4-1+1/2)+4.02;
//thumbs_position_z2=-kailh_choc_PG1350_height/2+3.98;
thumbs_position_z2=-kailh_choc_PG1350_height/2+4.08;
thumbs_position_x_left=kailh_choc_PG1350_dy+kailh_choc_PG1350_dy*5/2;
thumbs_position_x_right=kailh_choc_PG1350_dy-kailh_choc_PG1350_dy*5/2;
/* thumbs_position_z=-6-wall; */
thumbs_rotation_x=45;
thumbs_rotation_z=45;
thumb_vector=[thumbs_position_x_right,thumbs_position_y2,thumbs_position_z2];
thumb_vector_rotation=rotate([180+thumbs_rotation_x,0,thumbs_rotation_z]);
module thumb(tolerance=0, outlet=false, thumbs_rotation_x=thumbs_rotation_x, thumbs_rotation_z=thumbs_rotation_z,corner=false, thumbplate=false, raise=0, thumbplateholder=false){
     translate([thumbs_position_x_right,
		thumbs_position_y2,
		thumbs_position_z2]){
	  translate([0,
		     0,
		     0]){
	       rotate([180+thumbs_rotation_x,0,thumbs_rotation_z]){
		    rotate([0,-45*0,0]){
			 translate([0,0,0]){
			      if (outlet){ // outlet is raised key tolerance box 
				   translate([0,0,wall+tolerance*2]){
					if(thumbplate){kailh_choc_PG1350(thumbplate=thumbplate);}
					else if(thumbplateholder){thumbplateholder();}
					else {
					     kailh_choc_PG1350(tolerance=tolerance,corner=corner);
					}
				   }
			      }
			      else{
				   translate([0,0,raise]){
					if(thumbplate){kailh_choc_PG1350(thumbplate=thumbplate);}
					else if(thumbplateholder){thumbplateholder();}					
					else {
					     kailh_choc_PG1350(tolerance=tolerance,corner=corner);
					}
				   }
			      }
			 }
		    }
	       }
	  }
     }
}

module casecenter_mirror(){
     translate([+kailh_choc_PG1350_dy,0,0]){
	  mirror([1,0,0]){
	       translate([-kailh_choc_PG1350_dy,
			  0,
			  0])
	       {
		    children();
	       }
	  }
     }
}

module thumbs2(right=true, left=false, tolerance=0, outlet=false, thumbs_rotation_x=thumbs_rotation_x, thumbs_rotation_z=thumbs_rotation_z,corner=false, thumbplate=false, raise=0, thumbplateholder=false){
     case_inner_offset_z=-1.7;
     plate_case_inner_offset_z=-holderspace;
     if (right){
	  if(thumbplate){
	       intersection(){
		    thumb(tolerance=tolerance, outlet=outlet, thumbs_rotation_x=thumbs_rotation_x, thumbs_rotation_z=thumbs_rotation_z,corner=corner, thumbplate=true, raise=raise);
		    translate([0,0,plate_case_inner_offset_z]){case_inner(case_inner_hull_tolerance, outlets=true);}
	       }
	  }
	  else if(thumbplateholder){
	       difference(){
		    intersection(){
			 thumb(tolerance=tolerance, outlet=outlet, thumbs_rotation_x=thumbs_rotation_x, thumbs_rotation_z=thumbs_rotation_z,corner=corner, thumbplateholder=true, raise=raise);
			 translate([0,0,case_inner_offset_z]){case_inner(case_inner_hull_tolerance, outlets=true);}
		    }
		    pcbscrew_position(add=false);
		    translate([0,0,case_inner_offset_z]){case();}
		    case_cut(6);
	       }
	  }
	  else {
	       thumb(tolerance=tolerance, outlet=outlet, thumbs_rotation_x=thumbs_rotation_x, thumbs_rotation_z=thumbs_rotation_z,corner=corner, raise=raise);
	  }
     }
     if (left){
	  casecenter_mirror(){
	       if(thumbplate){
		    intersection(){
			 thumb(tolerance=tolerance, outlet=outlet, thumbs_rotation_x=thumbs_rotation_x, thumbs_rotation_z=thumbs_rotation_z,corner=corner, thumbplate=true, raise=raise);
			 translate([0,0,plate_case_inner_offset_z]){case_inner(case_inner_hull_tolerance, outlets=true);}
		    }
	       }
	       else if(!thumbplateholder){
		    thumb(tolerance=tolerance, outlet=outlet, thumbs_rotation_x=thumbs_rotation_x, thumbs_rotation_z=thumbs_rotation_z,corner=corner, raise=raise);
	       }
	  }
	  if(thumbplateholder){
	       difference(){
		    intersection(){
			 casecenter_mirror(){
			      thumb(tolerance=tolerance, outlet=outlet, thumbs_rotation_x=thumbs_rotation_x, thumbs_rotation_z=thumbs_rotation_z,corner=corner, thumbplateholder=true, raise=raise);
			 }
			 translate([0,0,case_inner_offset_z]){case_inner(case_inner_hull_tolerance, outlets=true);}
		    }
		    pcbscrew_position(add=false);
		    case_cut(6);
		    //translate([0,0,0*case_inner_offset_z]){case();}
	       }
	  }
     }
}


/* module thumbs(right=true, left=false, tolerance=0, outlet=false, thumbs_rotation_x=thumbs_rotation_x, thumbs_rotation_z=thumbs_rotation_z,corner=false){ */
/*      translate([0, */
/* 		thumbs_position_y, */
/* 		thumbs_position_z]){ */
/*      if (right){ */
/* 	  translate([thumbs_position_x_right, */
/* 		     0, */
/* 		     0]){ */
/* 	       rotate([180+thumbs_rotation_x*0,0,thumbs_rotation_z*0]){ */
/* 		    rotate([0,-45*0,0]){ */
/* 			 translate([-kailh_choc_PG1350_dx/2*0,0,-kailh_choc_PG1350_height*0]){ */
/* 			      if (outlet){ // outlet is raised key tolerance box */
/* 				   translate([0,0,wall+tolerance]){ */
/* 					kailh_choc_PG1350(tolerance=tolerance,corner=corner); */
/* 				   } */
/* 			      } */
/* 			      else{ */
/* 				   kailh_choc_PG1350(tolerance=tolerance,corner=corner); */
/* 			      } */
/* 			 } */
/* 		    } */
/* 	       } */
/* 	  } */
/*      } */
/*      if (left){ */
/* 	  translate([thumbs_position_x_left, */
/* 		     0, */
/* 		     0]){ */
/* 	       rotate([180+thumbs_rotation_x*0,0,-thumbs_rotation_z*0]){ */
/* 		    rotate([0,45*0,0]){ */
/* 			 translate([kailh_choc_PG1350_dx/2*0,0,-kailh_choc_PG1350_height*0]){ */
/* 			      if (outlet){ // outlet is raised key tolerance box */
/* 				   translate([0,0,wall+tolerance]){ */
/* 					kailh_choc_PG1350(tolerance=tolerance,corner=corner); */
/* 				   } */
/* 			      } */
/* 			      else{ */
/* 				   kailh_choc_PG1350(tolerance=tolerance,corner=corner); */
/* 			      } */
/* 			 } */
/* 		    } */
/* 	       } */
/* 	  } */
/*      } */
/*      } */
/* } */
/* thumbs(tolerance=0,left=true,corner=true); */
/* thumbs(tolerance=0,left=true,corner=false); */

//thumbs2(tolerance=0,left=true,outlet=true, corner=true);
//thumbs2(tolerance=0,left=true, outlet=true, corner=false);
		    
module pin_head(){
//    translate([0,0,-pin_h/2+pin_low]){
    translate([0,0,-pin_h/2+pin_high]){    
        cube([pin_w,pin_w,pin_h],center=true);
        }
    translate([0,0,-pin_block_width/2]){
        color("black"){
            cube([pin_block_width,pin_rm,pin_block_width],center=true);
            }
        }
    }

// arduino micro
arduino_width=17.9;
subpinlength=8.9;
usbwidth=7.7;
usbheight=2.5;
module arduino(tolerance=0, toleranceoptimized=0, usb=1){
     if( toleranceoptimized==0){
	  if(tolerance>0){
	       translate([-tolerance,-tolerance,0]){
		    cube([48.5+tolerance*2,
			  arduino_width+tolerance*2,
			  9.7+tolerance]);
		    translate([0,0,-subpinlength-tolerance]){
			 cube([48.5+tolerance*2,
			       arduino_width+tolerance*2,
			       subpinlength+tolerance]);
		    }
		    if (usb==1){
			 translate([-7+1-tolerance,
				    (arduino_width-usbwidth)/2,
				    1.6-tolerance]){
			      cube([7+tolerance,
				    usbwidth+tolerance*2,
				    usbheight+tolerance*2]);
			 }
		    }
	       }
	  }
	  else{
	       import("arduino_micro.stl", convexity=8);
	  }
     }
     if(toleranceoptimized>0){
	  translate([-tolerance,-tolerance,-tolerance/2]){
	       cube([48.5+tolerance*2,
		     arduino_width+tolerance*2,
		     4.5+tolerance]);
	       /* translate([0,0,-subpinlength-tolerance]){ */
	       /* 	    cube([48.5+tolerance*2, */
	       /* 		  arduino_width+tolerance*2, */
	       /* 		  subpinlength+tolerance]); */
	       /* } */
	       // space for jtag with trimmed pins
	       translate([40,2.5,tolerance/2]){
		    cube([7+tolerance*2,
			  5+tolerance*2,
			   /* 4.8]); */
		    9.7-3]); // jtag hight
	       }
	       translate([40,7.5,tolerance/2]){
		    cube([7+tolerance*2,
			  7.5+tolerance*2,
			  4.8]);
	       }
	       if (usb==1){
		    translate([-7+1-tolerance,
			       (arduino_width-usbwidth)/2,
			       1.6-tolerance/2]){
			 cube([7+tolerance,
			       usbwidth+tolerance*2,
			       usbheight+tolerance*2]);
		    }
	       }
	  }
     }
}
/* arduino(); */
/* arduino(tolerance=0, toleranceoptimized=1, usb=1); */

arduino_keypad_distance=0.3+2*tolerance1;
arduino_position_y=-0.6;
arduino_position_z=2.57;
module arduino_position(left=true, right=false, tolerance=0, toleranceoptimized=0, usb=1){
     if(left){
	  translate([kailh_choc_PG1350_dx*2+arduino_width+arduino_keypad_distance,arduino_position_y,arduino_position_z]){
	       rotate([0,0,90]){
		    arduino(tolerance, toleranceoptimized, usb);
	       }
	  }

     }
}

pcb_surplus_x=3;
module pcb(tolerance=0){
     translate([-9.95,71.67,-1.6]){
	  linear_extrude(height=1.6, convexity=10){
	       import("oskar_zither_pcb-brd.dxf", convexity=10);
	  }
     }
}

// logo
// source location
source_location_length=50;
source_location_height=0.5;
module source_location(source_location_length=source_location_length,source_location_height=source_location_height){
     rotate([0,180,0]){
	  resize([source_location_length,0,source_location_height], auto=true){
	       linear_extrude(source_location_height){
		    text("Licensed under");
		    translate([0,-15,0]){
			 text("CERN-OHL-S v2");
			 }
		    translate([0,-15*2,0]){
			 text("https://gitlab.com/");
		    }
		    translate([0,-15*3,0]){
			 text("teamoskar/oskar_zither");
		    }
	       }
	  }
     }
}
//!source_location();

// Wanted: rotate function on vectors
// http://forum.openscad.org/Wanted-rotate-function-on-vectors-td11218.html
// FUNCTION: is_String(x)
//   Returns true if x is a string, false otherwise.
function is_string(x) =
	x == undef || len(x) == undef
		? false // if undef, a boolean or a number
		: len(str(x,x)) == len(x)*2; // if an array, this is false

// FUNCTION: is_array(x)
//   Returns true if x is an array, false otherwise.
function is_array(x) = is_string(x) ? false : len(x) != undef;

function identity(d)        = d == 0 ? 1 : [for(y=[1:d]) [for(x=[1:d]) x == y ? 1 : 0] ];
function length(v)          = let(x=v[0], y=v[1], z=v[2]) sqrt(x*x + y*y + z*z);
function unit_vector(v)     = let(x=v[0], y=v[1], z=v[2]) [x/length(v), y/length(v), z/length(v)];
function skew_symmetric(v)  = let(x=v[0], y=v[1], z=v[2]) [[0, -z, y], [z, 0, -x], [-y, x, 0]];
function tensor_product1(u) = let(x=u[0], y=u[1], z=u[2]) [[x*x, x*y, x*z], [x*y, y*y, y*z], [x*z, y*z, z*z]];
function rotate(a, v)
 = is_array(a)
	? let(rx=a[0], ry=a[1], rz=a[2])
		  [[1, 0, 0],              [0, cos(rx), -sin(rx)], [0, sin(rx), cos(rx)]]
		* [[cos(ry), 0, sin(ry)],  [0, 1, 0],              [-sin(ry), 0, cos(ry)]]
		* [[cos(rz), -sin(rz), 0], [sin(rz), cos(rz), 0],  [0, 0, 1]]
	: let(uv=unit_vector(v))
	  cos(a)*identity(3) + sin(a)*skew_symmetric(uv) + (1 - cos(a))*tensor_product1(uv);
/* echo(rotate([0,0,-45])*[10,0,0]); */
/* vector=rotate([0,0,45])*[10,0,0]; */
/* echo(vector); */
/* translate(rotate([0,0,45])*[10,0,0]){cube([1,1,1]);} */



case_inner_hull_radius=tolerance1;
case_hull_tolerance=wall;
// kailh_choc_PG1350_cornervector definition in module case_inner and definition in module kailh_choc_PG1350
kailh_choc_PG1350_cornervector=[0,0,kailh_choc_PG1350_block_height/2-pg1350_ch/2-case_inner_hull_tolerance/2]+
     [-(keycap_x+case_inner_hull_tolerance)/2,(keycap_y+case_inner_hull_tolerance)/2,(kailh_choc_PG1350_block_height+pg1350_ch+case_inner_hull_tolerance)/2];
echo(kailh_choc_PG1350_cornervector=kailh_choc_PG1350_cornervector);
kailh_choc_PG1350_corner=thumb_vector+rotate([0,0,thumbs_rotation_z])*rotate([(180+thumbs_rotation_x)*1,0,0])*kailh_choc_PG1350_cornervector;
echo(kailh_choc_PG1350_corner=thumb_vector+rotate([0,0,thumbs_rotation_z])*rotate([(180+thumbs_rotation_x)*1,0,0])*kailh_choc_PG1350_cornervector);

module thumbcorner1(){
     cubelength=0.1;
     translate(thumb_vector+
     	       rotate([0,0,thumbs_rotation_z])*
     	       rotate([(180+thumbs_rotation_x)*1,0,0])
     	       *kailh_choc_PG1350_cornervector+[cubelength/2,-cubelength/2,cubelength/2]
     	  )
     {
     	  color("yellow")cube([cubelength,cubelength,cubelength],true);
     }
     // show corner on flat plane
     // thumbs2(tolerance=case_inner_hull_tolerance, left=false, corner=true); // debug
     *thumbs2(tolerance=case_inner_hull_tolerance, left=false, corner=false); // debug
}

kailh_choc_PG1350_cornervector2=[0,0,kailh_choc_PG1350_block_height/2-pg1350_ch/2-tolerance/2]+
     [(keycap_x+tolerance)/2,(keycap_y+tolerance)/2,(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2];
echo(kailh_choc_PG1350_cornervector2=kailh_choc_PG1350_cornervector2);
kailh_choc_PG1350_corner2=thumb_vector+rotate([0,0,thumbs_rotation_z])*rotate([(180+thumbs_rotation_x)*1,0,0])*kailh_choc_PG1350_cornervector2;
echo(kailh_choc_PG1350_corner2=thumb_vector+rotate([0,0,thumbs_rotation_z])*rotate([(180+thumbs_rotation_x)*1,0,0])*kailh_choc_PG1350_cornervector2);

module thumbcorner2(){
     cubelength=0.1;
     translate(thumb_vector+
	       rotate([0,0,thumbs_rotation_z])*
	       rotate([(180+thumbs_rotation_x)*1,0,0])
	       *kailh_choc_PG1350_cornervector2+[cubelength/2,-cubelength/2,cubelength/2]
	  )
     {
	  color("yellow")cube([cubelength,cubelength,cubelength],true);
     }
     // show corner on flat plane
     // thumbs2(tolerance=case_inner_hull_tolerance, left=false, corner=true); // debug
     *thumbs2(tolerance=case_inner_hull_tolerance, left=false, corner=false); // debug
}

hull_arduino_approximation=1.5;
bottom_z=arduino_position_z-subpinlength-case_inner_hull_tolerance*0;
bottom_y=-case_inner_hull_radius/2+hull_arduino_approximation;
module bottom(round=true){
     if (round){
	  translate([kailh_choc_PG1350_dy-bottomlength/2,
		     bottom_y,
		     bottom_z]){
	       minkowski(){
		    cube([bottomlength,
			  1,
			  -arduino_position_z+subpinlength+kailh_choc_PG1350_block_height-case_hull_tolerance+case_inner_hull_tolerance*2-1-case_inner_hull_radius*2]);
		    sphere(r=case_inner_hull_radius);
	       }
	  }
     }
     else{
	  translate([kailh_choc_PG1350_dy-bottomlength/2,
		     bottom_y-0.1,
		     bottom_z-case_inner_hull_radius]){
	       /* minkowski(){ */
		    cube([bottomlength,
			  0.1,
			  0.1]);
			  /* -arduino_position_z+subpinlength+kailh_choc_PG1350_block_height-case_hull_tolerance+case_inner_hull_tolerance*2-1-case_inner_hull_radius*2]); */
		    /* sphere(r=case_inner_hull_radius); */
	       /* } */
	  }
     }
}
//bottom(round=false);

alpha0=atan((kailh_choc_PG1350_corner[2]-(bottom_z-case_inner_hull_radius))/(kailh_choc_PG1350_corner[1]-bottom_y));
alpha1=-90+alpha0;

module support_plane(){
     /* thumbcorner2(); */
     hull(){
	  thumbcorner1();
	  casecenter_mirror(){
	       thumbcorner1();
	  }
	  bottom(round=false);


	  /* echo(alpha1=-90+atan((kailh_choc_PG1350_corner[2]-(bottom_z-case_inner_hull_radius))/(kailh_choc_PG1350_cornervector[1]-bottom_y))); */

	  /* echo(kailh_choc_PG1350_corner2_y=kailh_choc_PG1350_corner2[1]); */
	  /* echo(kailh_choc_PG1350_corner_y=kailh_choc_PG1350_corner[1]); */
	  /* echo(tan(90+alpha1)); */
	  delta_z=(kailh_choc_PG1350_corner2[1]-kailh_choc_PG1350_corner[1])*tan(90+alpha1);
	  /* echo(delta_z=(kailh_choc_PG1350_corner2[1]-kailh_choc_PG1350_corner[1])*tan(90+alpha1)); */

	  cubelength=0.1;
	  translate([kailh_choc_PG1350_corner2[0],
		     kailh_choc_PG1350_corner2[1],
		     kailh_choc_PG1350_corner[2]+delta_z]){
	       cube([cubelength,
		     cubelength,
		     cubelength]);
	  }
	  casecenter_mirror(){
	       translate([kailh_choc_PG1350_corner2[0],
			  kailh_choc_PG1350_corner2[1],
			  kailh_choc_PG1350_corner[2]+delta_z]){
		    cube([cubelength,
			  cubelength,
			  cubelength]);
	       }
	  }

     }
}
// support_plane();

// screw
// screw_l=12;
screw_l=16;
screw_d=3.5;
screw_inner_d= printer=="prusa i3" ? 0.8 : 3-0.5; // decrease if screws do not bite, or increase if screws are too tight
echo(screw_inner_d=screw_inner_d);
screw_dk=6;
screw_k=1.86;
screw_h=screw_l-screw_k;
screw_bottom_d=5;
screw_top_left_x=0;
screw_top_left_y=0;
wall_bottom_width=wall;

// pcb_cone_distance=2; // screw_l=12
pcb_cone_distance=4; // screw_l=16
module pcbscrew(outer_coat_z=-12, add=true, full=false, frontscrews=true, backscrews=true){
     outer_coat_h=-pcb_width-outer_coat_z-wall*2/3;
     screw_bottom_height=-pcb_width-outer_coat_z-wall-pcb_width-screw_k;

     color("lightblue"){
	  // outer coat
	  if(add){
	       if(full && backscrews){
		    translate([
				   0,
				   0,
				   -pcb_width/2-outer_coat_h/2]){
			 cylinder(h=outer_coat_h+pcb_width,d=(screw_bottom_d+4),center=true);
		    }
	       }
	       else if(backscrews) {
		    translate([
				   0,
				   0,
				   -pcb_width-outer_coat_h/2]){
			 cylinder(h=outer_coat_h,d=(screw_bottom_d+4),center=true);
		    }
	       }
	       // inlet supportring
	       if(backscrews){
		    translate([
				   0,
				   0,
				   outer_coat_z+wall/2+0.5]){
			 rotate([alpha0,0,0]){
			      cylinder(h=wall,d=(screw_dk+tolerance1)+0.1+wall*2,center=true);
			 }
		    }
     }
	  }
	  if(!add){
	       // inlet
	       translate([
			      0,
			      0,
			      outer_coat_z+wall/2]){
		    cylinder(h=wall+0.5,d2=(screw_dk+tolerance2)+0.1,d1=(screw_dk+tolerance1)+0.1+wall+0.5,center=true);
	       }

	       translate([
			      0,
			      0,
			      -screw_bottom_height/2-pcb_width]){
		    // inner diameter
		    cylinder(h=-pcb_width-(outer_coat_z)-2,d=screw_d,center=true);
		    // screw head skew
		    //translate([0,0,screw_bottom_height/2-screw_k/2-pcb_cone_distance]){cylinder(h=screw_k,d1=screw_dk+tolerance2,d2=3,center=true);}
		    translate([0,0,screw_bottom_height/2-screw_k/2-pcb_cone_distance]){
			 cylinder(h=screw_k,d1=screw_dk+tolerance2,d2=3,center=true);
		    }
	       }
	       // screw head inner diameter
	       translate([0,0,-pcb_width-pcb_cone_distance-screw_k-screw_bottom_height/2]){cylinder(h=screw_bottom_height,d=screw_dk+tolerance2,center=true);}
	  }
	  screw_front_h=keypad_size_z-wall;
	  if(add && frontscrews){
	       // thread

	       // thread outer coat
	       translate([0,
			  0,
			  (keypad_size_z-wall)/2]){
		    cylinder(h=screw_front_h,d=screw_bottom_d+4,center=true); // subtract
	       }
	  }
	  if(!add){
	       translate([0,
			  0,
			  (screw_front_h)/2]){
		    // thread inner most diameter
		    cylinder(h=screw_front_h,d1=screw_d,d2=screw_inner_d,center=true);
	       }
	  }
     }
}

screw_position1=[-6.35,68.75,0];
screw_position2=[-6.35,5.25,0];
screw_position3=[54.15,53.85,0];
screw_position4=[65.00,5.25,0];
module pcbscrew_position(add=true, frontscrews=true, backscrews=true, testprint=false){

     translate(screw_position1){pcbscrew(outer_coat_z=-17.64, add=add, frontscrews=frontscrews, backscrews=backscrews);}
     if(!testprint){
	  translate(screw_position2){pcbscrew(outer_coat_z=-10.34, add=add, frontscrews=frontscrews, backscrews=backscrews);}

	  translate(screw_position3){pcbscrew(outer_coat_z=-15.96, add=add, frontscrews=frontscrews, backscrews=backscrews);}
	  translate(screw_position4){pcbscrew(outer_coat_z=-10.34, add=add, full=true, frontscrews=frontscrews, backscrews=backscrews);}
	  // screw_positon5
	  if (add){
	       casecenter_mirror(){
		    translate(screw_position4){pcbscrew(outer_coat_z=-10.34, add=add, full=true, frontscrews=frontscrews, backscrews=backscrews);}
	       }
	  }
     }
}
/* difference(){ */
/*      pcbscrew_position(add=true); */
/*      pcbscrew_position(add=false); */
/* } */

module pcbscrew_print(frontscrews=false, backscrews=false){
     if(frontscrews){
	  rotate([0,180,0]){

	       difference(){
		    pcbscrew_position(add=true,frontscrews=frontscrews,backscrews=false,testprint=true);
		    pcbscrew_position(add=false,testprint=true);
	       }
	  }
     }
     if(backscrews){
	  rotate([-alpha0,0,0]){
	       difference(){
		    pcbscrew_position(add=true,frontscrews=false,backscrews=backscrews,testprint=true);
		    pcbscrew_position(add=false,testprint=true);
	       }
	  }
     }
}
// pcbscrew_print(frontscrews=true);
// pcbscrew_print(backscrews=true);

module pcbscrew_support(pcbscrew_position, front=false, back=false, top=false, bottom=false, angel=false, keypad=false){
     cubez= angel ? 26.5 : 20;
     cubex= screw_bottom_d+4-4;
     cubey= angel ? 26.5 : 20;

     rot_b= angel ? [45,0,45] : [0,0,0];
     rot_f= angel ? [45,0,90] : [0,0,0];
     rot = front ? rot_f : rot_b;

     transl_b = top ?
	  [0,-cubey/2,pcb_width+cubez/2+2] : // top
	  bottom ?
	  [0,cubey/2,pcb_width+cubez/2] : // bottom
	  [0,0,pcb_width+cubez/2+2]; // angel
     transl_f= top ?
	  [0,-cubey/2,0*pcb_width-cubez/2] : // top
	  bottom ?
	  [0,cubey/2,0*pcb_width-cubez/2-7.4] : // bottom
	  [0,0,0*pcb_width-cubez/2-2]; // angel
     transl = front ? transl_f : transl_b;

     if(keypad && front){
	  cubekeypadz=8;
	  cubekeypadx=5;
	  difference(){
	       translate(pcbscrew_position-[-cubekeypadx/2,0,-cubekeypadz/2-tolerance]){
		    cube([cubekeypadx,cubex,cubekeypadz],center=true);
	       }
	       pcbscrew_position(add=false);
	  }
	  if(bottom){
	       cubey=pcbscrew_position[1];
	       cubez=4;
	       difference(){
		    translate(pcbscrew_position-[-cubex/4,cubey/2,-usblower_z-slitwidth-cubez/2]){
			 cube([cubex*1.5,cubey,cubez],center=true);
		    }
		    pcbscrew_position(add=false);
	       }
	  }
     }

     else if (!keypad){
	  intersection(){
	       case_inner(case_inner_hull_tolerance, outlets=true);
	       difference(){
		    translate(pcbscrew_position-transl){
			 rotate(rot){
			      cube([cubex,cubey,cubez],center=true);
			      if (back && bottom){echo(transl);}
			 }
		    }
		    pcbscrew_position(add=false);
		    if(angel){
			 translate(pcbscrew_position+[0,0,front ? -cubez/2 : -pcb_width+cubez/2]){rotate([0,0,rot[2]])cube([cubex,cubey,cubez],center=true);};
		    }
	       }
	  }
     }
}

module pcbscrew_support_position(front=false,back=false){
     pcbscrew_support(pcbscrew_position=screw_position1,front=front,back=back,top=true);
     pcbscrew_support(pcbscrew_position=screw_position1,front=front,back=back,keypad=true);
     pcbscrew_support(pcbscrew_position=screw_position2,front=front,back=back,keypad=true,bottom=true);
     pcbscrew_support(pcbscrew_position=screw_position3,front=front,back=back,angel=true);
     pcbscrew_support(pcbscrew_position=screw_position4,front=front,back=back,bottom=true);
     casecenter_mirror(){
	  pcbscrew_support(pcbscrew_position=screw_position4,front=front,back=back,bottom=true);
     }
}
//pcbscrew_support_position();

module keypad_support(front=false,back=false){
     cubey=10;
     if(front){
	  tolerance=keypadtolerance;
	  intersection(){
	       difference(){
		    translate([-wall,tolerance,tolerance]){
			 cube([2*kailh_choc_PG1350_dy+wall*2,
				4*kailh_choc_PG1350_dx+wall-tolerance,
				kailh_choc_PG1350_block_height-tolerance]);
		    }
		    keypad(tolerance=keypadtolerance);
	       }
	       case_inner(case_inner_hull_tolerance, outlets=true);
	  }
	  // close keypadframe gap to bottom
	  intersection(){
	       difference(){
		    translate([-wall,-wall,usblower_z+slitwidth+wall*0+0*tolerance]){
			 cube([2*kailh_choc_PG1350_dy+wall*2,
			       4*kailh_choc_PG1350_dx+wall*2,
			       kailh_choc_PG1350_block_height-tolerance-(usblower_z+slitwidth)]);
		    }
		    keypad(tolerance=keypadtolerance);
	       }
	       case_inner(case_inner_hull_tolerance, outlets=true);
	  }

	  // support of case on top of pcb with support on back
	  difference(){
	       translate([2*kailh_choc_PG1350_dy+keypadtolerance/4,tolerance,0]){
		    cube([wall,cubey-tolerance,kailh_choc_PG1350_block_height-0.1]);
	       }
	       keypad(tolerance=keypadtolerance);
	  }
	  // round frame for keypad at front bottom
	  minkh=wall+0.45;
	  difference(){
	       translate([0,0.05,kailh_choc_PG1350_block_height-minkh-0.025]){
		    minkowski(){
			 cube([2*kailh_choc_PG1350_dy+keypadtolerance*0,1+kailh_choc_PG1350_dx*4-wall,1]);
			 cylinder(h=minkh-1,d=wall*2);
		    }
	       }
	       case_inner(case_inner_hull_tolerance, outlets=true);
	  }
     }
     if(back){
	  cubez=20;
	  intersection(){
	       translate([2*kailh_choc_PG1350_dy+keypadtolerance/4,0,-pcb_width-cubez]){
		    cube([wall,cubey,cubez]);
	       }
	       case_inner(case_inner_hull_tolerance, outlets=true);
	  }
     }
}
/* keypad_support(front=true); */
/* keypad_support(back=true); */

bottomlength=thumbs_position_x_left-thumbs_position_x_right-case_inner_hull_radius*2+35;
keypadtolerance=case_inner_hull_tolerance+1; // tolerance is actually 1/2 * kaypadtolerance
module case_inner(case_inner_hull_tolerance=case_inner_hull_tolerance, outlets=true,corner=false){
     hull(){
	  thumbs2(tolerance=case_inner_hull_tolerance, left=true);
	  support_plane();
	  bottom();
	  //keypad(tolerance=case_hull_tolerance*0.1);
	  //arduino_position(left=true, tolerance=case_inner_hull_tolerance*0.1, toleranceoptimized=1, usb=0);
     }

     // outlets
     if (outlets){
	  translate([0,0,wall+case_inner_hull_tolerance]){
	       keypad(tolerance=keypadtolerance);
	  }
	  thumbs2(left=true, tolerance=thumbkeycaptolerance, outlet=true);
	  arduino_position(left=true, tolerance=case_inner_hull_tolerance/2, toleranceoptimized=1, usb=1);
     }
}

// #arduino_position(left=true, tolerance=case_inner_hull_tolerance/2, toleranceoptimized=1, usb=1);
/* casecenter_mirror(){ */
/*      #arduino_position(left=true, tolerance=case_inner_hull_tolerance/2, toleranceoptimized=1, usb=1); */
/*      } */

//#case_inner();


// thumbs(tolerance=0,left=true, corner=true, outlet=true);
// color("red"){
// case_inner(outlets=false);
// }

module case_outer(r=wall){
     //case_inner(case_hull_tolerance, outlets=false);
     minkowski(){
	  case_inner(case_inner_hull_tolerance, outlets=false);
	  //sphere(r=case_hull_tolerance-case_inner_hull_tolerance);
	  sphere(r=r);
     }
}
//case_inner(case_inner_hull_tolerance, outlets=false);
// case_outer();
module case_cut_right(slitwidth=tolerance){
     delta=3;
     deltaradius=-1.7;
     /* difference(){ */
     hull(){
	  translate([kailh_choc_PG1350_dy-bottomlength/2-case_inner_hull_radius-wall,
		     bottom_y-case_inner_hull_radius+0.1,
		     arduino_position_z+(1.6-case_inner_hull_tolerance*0)+(usbheight+case_inner_hull_tolerance*0-0.26)]){
	       rotate([alpha0+deltaradius,0,0]){
		    cube([wall+delta,
			  71.5,
			  slitwidth]);
	       }
	  }
          // top but short
          translate(kailh_choc_PG1350_corner3+[-1.3,-2,-2.0]){
	       cube([60,5,slitwidth]);
	  }
     }
}
//case_cut_right(slitwidth=1);

// top in case_cut
kailh_choc_PG1350_cornervector3=[0,0,kailh_choc_PG1350_block_height/2-pg1350_ch/2-tolerance/2]+
     [(keycap_x+tolerance)/2,-(keycap_y+tolerance)/2,(kailh_choc_PG1350_block_height+pg1350_ch+tolerance)/2];
kailh_choc_PG1350_corner3=thumb_vector+rotate([0,0,thumbs_rotation_z])*rotate([(180+thumbs_rotation_x)*1,0,0])*kailh_choc_PG1350_cornervector3;

slitwidth=tolerance;
bottomslitlower_z=arduino_position_z+(1.6-case_inner_hull_tolerance*0)+(usbheight+case_inner_hull_tolerance*0-0.26);

// keypad to usb
center=kailh_choc_PG1350_dy;
usedtolerance=case_inner_hull_tolerance/2;
usbcutlength=usbwidth+usedtolerance*2;
centerdistance=kailh_choc_PG1350_dy*0+kailh_choc_PG1350_dx+arduino_keypad_distance+usedtolerance+(arduino_width-usbwidth)/2+slitwidth;
cutlength=centerdistance*2;
usblower_z=arduino_position_z-usedtolerance/2+1.6-usedtolerance/2+0*slitwidth-0.26;
slitwidthmax=15;
cutdepth=bottomslitlower_z+slitwidth-usblower_z;


module case_cut(slitwidth=tolerance,all=false){
     // bottom

     // keypad to usb
     slitwidthoptional= all ? slitwidthmax : slitwidth;
     walloptional= all ? slitwidthmax : wall;

     translate([kailh_choc_PG1350_dy-bottomlength/2-wall-case_inner_hull_radius,
		bottom_y-case_inner_hull_radius-wall,
		bottomslitlower_z]){
	  cube([center-centerdistance -(kailh_choc_PG1350_dy-bottomlength/2-wall-case_inner_hull_radius), // bottomlength+2*(case_inner_hull_radius+wall),
		walloptional+0.25,
		slitwidthoptional]);
     }

     translate([center+centerdistance,
     		bottom_y-case_inner_hull_radius-wall,
     		bottomslitlower_z]){
     	  cube([center-centerdistance -(kailh_choc_PG1350_dy-bottomlength/2-wall-case_inner_hull_radius), // bottomlength+2*(case_inner_hull_radius+wall),
     		walloptional+0.25,
     		slitwidthoptional]);
     }

     translate([center-centerdistance,
     		     bottom_y-case_inner_hull_radius-wall,
     		     usblower_z]){
	  cube([slitwidth,walloptional+0.25,cutdepth]);}
     translate([center-centerdistance,
     		 bottom_y-case_inner_hull_radius-wall,
		 usblower_z]){
	  cube([cutlength,walloptional+0.25,slitwidthoptional]);}
     translate([center+cutlength-slitwidth-centerdistance,
     		 bottom_y-case_inner_hull_radius-wall,
		 usblower_z]){
	  cube([slitwidth,walloptional+0.25,cutdepth]);}

     if(all){
	  slitwidthmax=15;
	  hull(){
	       case_cut_right(slitwidth=slitwidthmax);
	       casecenter_mirror(){case_cut_right(slitwidth=slitwidthmax);}
	  }
     }
     else{
	  case_cut_right(slitwidth=slitwidth);
	  casecenter_mirror(){case_cut_right(slitwidth=slitwidth);}
     }
}
// case_cut(tolerance);
// #case_cut(all=true);
//hull()case_cut(slitwidth=15);

module case_cut_overlap(front=false, back=false, in=false, out=false){
     movez = (back && !front) ? -tolerance/3 : tolerance/2;
     if (out){
	  difference(){
	       intersection(){
		    translate([0,0,movez]){
			 case_cut(tolerance);
		    }
		    case_outer();
	       }
	       case_outer(wall*2/3);
	       case_inner(case_inner_hull_tolerance, outlets=true);
	  }
     }
     else if (in){
	  difference(){
	       intersection(){
		    translate([0,0,movez]){
			 case_cut(tolerance);
		    }
		    case_outer(wall/3);
	       }
	       //case_outer(wall*2/3);
	       case_inner(case_inner_hull_tolerance, outlets=true);
	  }
     }
}

module case(screws=true){
     difference(){
	  union(){
	       difference(){
		    case_outer();
		    case_inner(case_inner_hull_tolerance, outlets=true);
		    //case_slits();
		    //insight
		    //translate([-7,0,-20]){cube([100,80,70]);}
	       }
	       if(screws){
		    pcbscrew_position(add=true);
		    //pcbscrew_support_position(front=true);
		    //pcbscrew_support_position(back=true);
	       }
	  }
	  if(screws){
	       pcbscrew_position(add=false);
	  }
	  //case_cut();
     }
}
//case();

module case_n_thumbplateholder(screws=true){
     case(screws=screws);
//thumbs2(tolerance=0,left=true, corner=false, raise=(wall+tolerance*2));
//thumbs2(tolerance=0,left=true, corner=false, thumbplate=true, raise=(wall+tolerance*2));
     thumbs2(tolerance=0,left=true, corner=false, thumbplateholder=true, raise=(wall+tolerance*2));
}

module case_front_cut(screws=true){
     difference(){
	  union(){
	       intersection(){
		    case(screws=false);
		    translate([0,0,0.31]){
			 case_cut(all=true);
		    }
	       }
	  }
	  pcbscrew_position(add=false);
	  case_cut(slitwidth=tolerance);
     }
     difference(){
	  pcbscrew_position(add=true,backscrews=false);
	  pcbscrew_position(add=false);
     }
     pcbscrew_support_position(front=true);
     keypad_support(front=true);
     case_cut_overlap(front=true, out=true);
}
//#case_front_cut();

module print_front(){
     rotate([-180,0,90+30*0]){case_front_cut();}
}
//print_front();

module print_tplate(){
     thumbplate();
     translate([30,0,0]){
	  thumbplate();
     }
}

module case_back_cut(){
     difference(){
	  union(){
	       difference(){
		    case(screws=false);
		    translate([0,0,0.26]){
			 case_cut(all=true);
		    }
	       }
	       pcbscrew_position(add=true,frontscrews=false);
	       pcbscrew_support_position(back=true);
	       keypad_support(back=true);
	       intersection(){
		    thumbs2(tolerance=0,left=true, corner=false, thumbplateholder=true, raise=(wall+tolerance*2));
		    case_inner(case_inner_hull_tolerance, outlets=false);
	       }
	  }
	  pcbscrew_position(add=false);
     }
     case_cut_overlap(back=true, in=true);
}
//#case_back_cut();

module level_back_case(){
     translate([0,0,9.17]){
	  rotate([-alpha0,0,0]){case_back_cut();}
     }
}

module level_back_cutblock(){
     translate([-50,0,-10]){
	  cube([140,100,10]);
     }
}

module level_back(test=false){
     if(test){
	  color("red")
	  intersection(){
	       level_back_case();
	       level_back_cutblock();
	  }
     }
     else{
	  difference(){
	       level_back_case();
	       level_back_cutblock();
	       // logo
	       // source location
	       /* source_location_length=90; */
	       /* source_location_height=0.7; */
	       /* translate([kailh_choc_PG1350_dy+source_location_length/2,43,source_location_height]){ */
	       /* 	    #source_location(source_location_length,source_location_height); */
	       /* } */
	  }
     }
}
//level_back(test=true);

module print_back(){
     rotate([0,0,90]){
	  level_back(test=false);
     }
}

//%case();

// #keypad();
// thumbs2(tolerance=0,left=true, corner=false, raise=(wall+tolerance*2));
// #thumbs2(tolerance=0,left=true, corner=false, thumbplate=true, raise=(wall+tolerance*2));
/* thumbs2(tolerance=0,left=true, corner=false, thumbplateholder=true, raise=(wall+tolerance*2)); */
// thumbs2(tolerance=tolerance1*1,left=false, outlet=true, corner=true);
/* #keypad(tolerance=1); */
/* thumbs(); */
/* //thumbs(tolerance=1); */
/* %smartphone(); */
/* #smartphone(tolerance=1); */
/* //pcb(); */
/* //pcb(tolerance=1); */
// #arduino_position(left=true, right=false,tolerance=0,toleranceoptimized=0, usb=0);
// #arduino_position(left=true, right=false,tolerance=0,toleranceoptimized=1, usb=1);
// #pcb();


//#case_front_cut();
difference(){
     *case_n_thumbplateholder();
     union(){
          *case_front_cut();
	  *case_back_cut();
	  }
     debug=false;
     if(debug){
	  translate([-25,-10,-20]){
	       minkowski(){
		    cube([57+13*0,80,40]);
		    sphere(r=case_hull_tolerance-case_inner_hull_tolerance);
	       }
	  }
     }
}

*print_tplate();
*print_front();
*print_back();
