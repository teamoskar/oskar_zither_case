# oskar_zither_case
Case for Oskar Zither.

[Oskar Zither](https://gitlab.com/teamoskar/oskar_zither) is a open-source, mobile braille-keyboard.

The arrangement of 8 keys in a braille cell block and two additional keys allows Oskar Zither to be controlled without a supporting surface.

![Oskar Zither front with 8 keys](/images/zitherfront.jpg)

# Build
[openscad](https://www.openscad.org) zither.scad

find print_front(), print_back(), print_tplate() near the end of zither.scad and disable or enable output with [disable modifier](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Modifier_Characters#Disable_Modifier)

# Print
you can download the stl files for printing [Oskar Zither Case Version 0.1](https://gitlab.opensourceecology.de/verein/projekte/cab/oskar/oskar_zither_case/-/releases) as a [zip-file](https://gitlab.com/teamoskar/oskar_zither_case/uploads/fea9613c22b043cad0bc2a972f7b387f/OskarZitherCase_v0_1.zip).
print tplate twice.

30% infill density,

60 mm/s print speed,

0.2 mm resolution,

5 hours print duration,

60 g (20 m, 1.75 mm diameter) PLA filament

front (2 hours), back (2h:45m), 2x tplate (10 minuts)

# Contact
[https://oskars.org](https://oskars.org)
Johannes Strelka-Petz <johannes_at_oskars.org>

# Copyright & License
	SPDX-FileCopyrightText: 2022 Johannes Strelka-Petz <johannes_at_oskars.org>
	SPDX-License-Identifier: CERN-OHL-S-2.0+
     ------------------------------------------------------------------------------
    | Copyright Johannes Strelka-Petz 2022.                                        |
    |                                                                              |
    | This source describes Open Hardware and is licensed under the CERN-OHL-S v2  |
    | or any later version.                                                        |
    |                                                                              |
    | You may redistribute and modify this source and make products using it under |
    | the terms of the CERN-OHL-S v2 or any later version                          |
    | (https://ohwr.org/cern_ohl_s_v2.txt).                                        |
    |                                                                              |
    | This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,          |
    | INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A         |
    | PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 or any later version        |
    | for applicable conditions.                                                   |
    |                                                                              |
    | Source location: https://gitlab.com/teamoskar/oskar_zither_case              |
     ------------------------------------------------------------------------------
